## biloba-user 11 RP1A.200720.011 V12.5.4.0.RCUEUXM release-keys
- Manufacturer: xiaomi
- Platform: mt6768
- Codename: biloba
- Brand: Redmi
- Flavor: biloba-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: V12.5.4.0.RCUEUXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/biloba_eea/biloba:11/RP1A.200720.011/V12.5.4.0.RCUEUXM:user/release-keys
- OTA version: 
- Branch: biloba-user-11-RP1A.200720.011-V12.5.4.0.RCUEUXM-release-keys
- Repo: redmi_biloba_dump_16416


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
